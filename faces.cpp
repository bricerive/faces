#include "sqlite3.h"
#include "utf8.h"
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <string>
#include <vector>
#include <stack>
#include <cstdio>
#include <iostream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;

static const double kWinHeight=500;
static const double kWinWidth=500;

typedef vector<string> Strings;
typedef vector< vector<string> > Response;

static const sf::Color kBackground(200, 200, 255);
static const sf::Color kTextColor(0, 0, 0);

class SqliteDb
{
public:
    SqliteDb(const string &dbPath)
    {
        if (sqlite3_open(dbPath.c_str(), &db)!=SQLITE_OK) {
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
            exit(1);
        }
    }
    ~SqliteDb()
    {
        sqlite3_close(db);
    }

    void Query(const boost::format &format)
    {
        Query(format.str());
    }
    
    void Query(const string &query)
    {
        char *zErrMsg = 0;
        int rc = sqlite3_exec(db, query.c_str(), &CallbackGlue, this, &zErrMsg);
        if( rc!=SQLITE_OK )
        {
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
        }
    }
    bool EmptyResponse()const {return response.empty(); }
    Response response;
private:
    static int CallbackGlue(void *param, int argc, char **argv, char **azColName)
    {
        SqliteDb *obj(reinterpret_cast<SqliteDb *>(param));
        return obj->Callback(argc, argv, azColName);
    }
    // called for each row of the query reply
    int Callback(int argc, char **argv, char **azColName)
    {
        vector<string> row;
        for (int i=0; i<argc; i++)
            row.push_back(argv[i]? argv[i]: "");
        response.push_back(row);
        return 0;
    }
    sqlite3 *db;
};

static void Alert(const string &msg)
{
    // Create the window for the dialog
    sf::RenderWindow alertWindow(sf::VideoMode(500, 50), "Alert", sf::Style::None);
    
    // Load the text font
    sf::Font theFont;
    if (!theFont.loadFromFile("ProFontWindows.ttf"))
        return;
    
    // Define a string for displaying message
    sf::Text msgString;
    msgString.setString(msg);
    msgString.setFont(theFont);
    msgString.setPosition(0.f, 0.f);
    msgString.setColor(kTextColor);
    
    string keys;
    
    // Start the game loop
    while (alertWindow.isOpen())
    {
        // Process events
        sf::Event Event;
        while (alertWindow.pollEvent(Event))
        {
            // Close window : exit
            if (Event.type == sf::Event::Closed || Event.type == sf::Event::KeyPressed || Event.type == sf::Event::MouseButtonPressed)
                alertWindow.close();
        }

        // Clear the window
        alertWindow.clear(kBackground);
        
        // Draw interface strings
        alertWindow.draw(msgString);
        
        // Finally, display the rendered frame on screen
        alertWindow.display();
    }
    
    return;
}

static bool LocateIPhotoLibrary(string &path)
{
    // look into iApps plist to find last ueed location for iPhoto Library
    //string readCmd = "/usr/libexec/PlistBuddy -c \"Print :iPhotoRecentDatabases\" com.apple.iApps.plist";
    string readCmd = "defaults read com.apple.iApps iPhotoRecentDatabases";
    
    FILE* pipe = popen(readCmd.c_str(), "r");
    if (!pipe) return false;
    char buffer[128];
    string output;
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            output += buffer;
    }
    pclose(pipe);
    
    // The output looks like this:
    // (
    //    "file://localhost/Volumes/Brice/Pictures/iPhoto%20Library/AlbumData.xml"
    // )

    int first = output.find("file://localhost")+16;
    int last = output.find("/AlbumData.xml")-1;
    path = output.substr(first, last-first+1);
    
    size_t br=0;
	while ((br = path.find("%20", br)) != string::npos)
		path.replace(br, 3, " "); 
    
    return true;
}

static string IPhotoLibPath()
{
    return "/Users/brice/Pictures/Photos Library.photoslibrary";
    
    static string lastLibrary;
    if (lastLibrary.empty()) {
        if (!LocateIPhotoLibrary(lastLibrary)) {
            Alert("Cannot find iPhoto Library!!!");
            exit(1);        
        }
    }

    fs::path libPath(lastLibrary);
    if (!exists(libPath)) {
        libPath = "/Users/brice/Pictures/Photos Library.photoslibrary";
        if (!exists(libPath)) {
            Alert("Cannot find iPhoto Library!!!");
            exit(1);
        }
    }
    return libPath.string();
}

static string IPhotoFacesDb()
{
    return IPhotoLibPath()+"/Database/apdb/Faces.db";
}

static string IPhotoLibraryDb()
{
    return IPhotoLibPath()+"/Database/apdb/Library.apdb";
}

static string IPhotoThumbNails()
{
    return IPhotoLibPath()+"/Thumbnails/";
}

static void GetAllNames(Response &names)
{
    // open the Faces db in iPhoto
    SqliteDb facesDb(IPhotoFacesDb());

    // get the faceKey for that name
    facesDb.Query("SELECT name, fullName FROM RKFaceName ORDER BY name;");
    
    if (facesDb.EmptyResponse())
    {
        fprintf(stderr, "No name found\n");
        exit(1);
    }
    
    names = facesDb.response;
}


static void GetMatchingNames(const string &name, Strings &names)
{
    Response faceImages;
    
    SqliteDb facesDb(IPhotoFacesDb());
    
    // get the faceKey for that name
    Response &response(facesDb.response);
    facesDb.Query(format("SELECT name FROM RKFaceName WHERE name='%s';")%name);
    if (response.empty())
        facesDb.Query(format("SELECT name FROM RKFaceName WHERE name LIKE '%%%s%%';")%name);
    
    names.clear();
    for (Response::const_iterator i=response.begin(); i!=response.end(); ++i)
    {
        names.push_back((*i)[0]);
    }
}

static void FindNames(const Response &allNames, const string &keys, Strings &selectedNames)
{
    GetMatchingNames(keys, selectedNames);
}



//static sf::Unicode::UTF32String Utf8ToString(const string &utf8) 
//{ 
//    //    sf::Unicode::UTF32String utf32; 
//    //    const char *c=utf8.c_str();
//    //    sf::Unicode::UTF8ToUTF32(c, c+utf8.size(), std::back_inserter(utf32)); 
//    //    return sf::Unicode::Text(utf32); 
//    
//    
//    sf::Unicode::UTF32String utf32line; 
//    utf8::utf8to32(utf8.begin(), utf8.end(), back_inserter(utf32line)); 
//    return utf32line;
//}


static string SelectName()
{
    Response allNames;
    GetAllNames(allNames);
    
    // Create the window for the dialog
    sf::RenderWindow selectWindow(sf::VideoMode(800, 600), "Name selection", sf::Style::None);
    
    // Load the text font
    sf::Font theFont;
    if (!theFont.loadFromFile("ProFontWindows.ttf"))
        return "";
    
    // Define a string for displaying current string
    sf::Text labelKeys;
    labelKeys.setString("Name: ");
    labelKeys.setFont(theFont);
    labelKeys.setPosition(0.f, 0.f);
    labelKeys.setColor(kTextColor);
    
    // Define a string for displaying matching names
    sf::Text labelNames;
    labelNames.setFont(theFont);
    labelNames.setPosition(0.f, 30.f);
    labelNames.setCharacterSize(12);
    
    Strings selectedNames;
    string keys;
    
    // Start the game loop
    while (selectWindow.isOpen())
    {
        // Process events
        sf::Event Event;
        while (selectWindow.pollEvent(Event))
        {
            // Close window : exit
            if (Event.type == sf::Event::Closed)
                selectWindow.close();
            
            if (Event.type == sf::Event::KeyPressed)
            {
                // Return key -> done with selection
                if (Event.key.code == sf::Keyboard::Key::Return)
                {
                    if (selectedNames.size()==1)
                    {
                        selectWindow.close();
                        return selectedNames[0];
                    }
                }

                // delete key -> back to previous name
                if (Event.key.code == sf::Keyboard::Key::BackSpace)
                {
                    if (!keys.empty())
                    {
                        
                        keys = keys.substr(0, keys.size()-1);
                        FindNames(allNames, keys, selectedNames);
                    }
                }
                
                // Other key -> try to expand the name
                if ((Event.key.code>=sf::Keyboard::Key::A && Event.key.code<=sf::Keyboard::Key::Z) || Event.key.code==sf::Keyboard::Key::Space|| Event.key.code==sf::Keyboard::Key::Dash)
                {
                    int glyph = (Event.key.code - sf::Keyboard::Key::A);
                    char c = 'a';
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::RShift))
                        c = 'A';
                    int key = c+glyph;
                    if (Event.key.code==sf::Keyboard::Key::Space)
                        key = ' ';
                    if (Event.key.code==sf::Keyboard::Key::Dash)
                        key = '-';
                    keys += key;
                    FindNames(allNames, keys, selectedNames);
                }
            }
        }
        
        
        // Clear the window
        selectWindow.clear(kBackground);
        
        // Draw interface strings
        string nameList;
        for (Strings::iterator i=selectedNames.begin(); i!=selectedNames.end(); ++i)
        {
            nameList += *i + '\n';
        }
        sf::String Str = sf::String::fromUtf8(nameList.begin(), nameList.end());
        sf::Text st;
        st.setString(Str);
        labelNames.setString(Str);
        selectWindow.draw(labelNames);
        labelKeys.setString("Name: "+keys);
        selectWindow.draw(labelKeys);
        
        // Finally, display the rendered frame on screen
        selectWindow.display();
    }
    
    return "";
}

static void copyDir(const boost::filesystem::path& source, const boost::filesystem::path& dest)
{
    namespace fs = boost::filesystem;
    try {
        // Check whether the function call is valid
        if(!fs::exists(source) || !fs::is_directory(source)) {
            std::cerr << "Source directory " << source.string().c_str()
            << " does not exist or is not a directory." << std::endl;
            return;
        }
        if(fs::exists(dest)) {
            std::cerr << "Destination directory " << dest.string().c_str()
            << " already exists." << std::endl;
            return;
        }
        // Create the destination directory
        if(!fs::create_directory(dest)) {
            std::cerr << "Unable to create destination directory "
            << dest.string().c_str() << std::endl;
            return;
        }
    }
    catch(fs::filesystem_error& e) {
        std::cerr << e.what() << std::endl;
        return;
    }
    // Iterate through the source directory
    for(fs::directory_iterator it(source); it != fs::directory_iterator(); it++)
    {
        try {
            fs::path current(it->path());
            if(fs::is_directory(current)) {
                // Found directory: Recursion
                copyDir(current, 
                        dest.string() + "/" + current.filename().string());
            }
            else {
                // Found file: Copy
                fs::copy_file(current,
                              fs::path(dest.string() + "/" + current.filename().string()));
            }
        }
        catch(fs::filesystem_error& e) {
            std::cerr << e.what() << std::endl;
        }
    }
}

static void GetThumbPathsFast(string &name, Strings &paths)
{
    // do we have data already?
    bool weHaveData=false;
    filesystem::path thumbsPath("thumbs");
    if (filesystem::exists(thumbsPath))
    {
        filesystem::directory_iterator end_itr;
        for (filesystem::directory_iterator itr(thumbsPath); itr!=end_itr; ++itr)
        {
            filesystem::path crtPath = itr->path();
            if (!weHaveData)
            {
                weHaveData = true;
                string fileName = crtPath.filename().string();
                name = fileName.substr(0, fileName.size()-6);
            }
            paths.push_back(crtPath.c_str());
        }
    }
    
    if (!weHaveData)
    {
        // Select the name we want
        name = SelectName();

        SqliteDb facesDb(IPhotoFacesDb());
            
        // get the faceKey for that name
        Response &response(facesDb.response);
        facesDb.Query(format("SELECT faceKey, name FROM RKFaceName WHERE name='%s';")%name);
        if (response.empty())
            facesDb.Query(format("SELECT faceKey, name FROM RKFaceName WHERE name LIKE '%%%s%%';")%name);
        if (response.size()!=1)
        {
            fprintf(stderr, "Found several faces: %lu\n", response.size());
            for (Response::const_iterator i=response.begin(); i!=response.end(); ++i)
                fprintf(stderr, "\t%s\n", (*i)[1].c_str());
            exit(1);
        }
        string faceKey = response[0][0];
        response.clear();
        
        // get the images for this faceKey
        facesDb.Query(format("SELECT masterUuid, faceIndex FROM RKDetectedFace WHERE faceKey=%s ORDER BY masterUuid;")%faceKey);
        Response faceImages = response;
        
        SqliteDb libraryDb(IPhotoLibraryDb());
        
        string query = "SELECT uuid, imagePath FROM RKMaster WHERE uuid IN (";
        for (Response::const_iterator i=faceImages.begin(); i!=faceImages.end(); ++i)
        {
            if (i!=faceImages.begin())
                query += ',';
            query += string("'") + (*i)[0] + "'";
        }
        query += ") ORDER BY uuid;";
        libraryDb.Query(query);
        
        Response faceThumbs=libraryDb.response;
        
        
        int nbFound = faceThumbs.size();
        int skip=0;
        
        filesystem::path src = filesystem::current_path().parent_path().parent_path();
        filesystem::path dst = src.parent_path() / (name+".app");
        //filesystem3::copy_directory(src, dst);
        copyDir(src,dst);
        filesystem::path thumbsPath = dst / "Contents" / "Resources" / "thumbs";
        
        filesystem::create_directory(thumbsPath);
        
        
        int index=0;
        for (int i=0; i<faceThumbs.size(); i++)
        {
            while (faceThumbs[i][0] != faceImages[i+skip][0] && i+skip<nbFound)
                skip++;
            string &path(faceThumbs[i][1]);
            //string fileName = path.substr(0, path.size()-4)+"_face"+faceImages[i+skip][1]+".jpg";
            string fileName = path;
            string faceThumbPath = string(IPhotoThumbNails())+fileName;
            
            if (boost::filesystem::exists(faceThumbPath))
            {                
                string dstName = name + "_" + boost::lexical_cast<string>(index++) + ".jpg";
                filesystem::path dstPath = thumbsPath / dstName;
                try {
                    if (!exists(dstPath))
                        filesystem::copy_file(faceThumbPath, dstPath);
                    paths.push_back(dstPath.string());
                } catch (...) {}
            }
        }
        
        string cmd = string("./seticon \"" + paths[0] + "\" \"" + dst.string()) + "\"";
        ::system(cmd.c_str());
        cmd = string("echo ")+cmd;
        ::system(cmd.c_str());
    }
}

static void ShowImage(sf::RenderWindow &window, sf::Image &faceImage, sf::Texture &faceTexture, sf::Sprite &faceSprite, Strings &paths, int imageIndex)
{
    if (faceImage.loadFromFile(paths[imageIndex]))
    {
        sf::Vector2u windowSize = window.getSize();
        unsigned int windowWidth=windowSize.x;
        unsigned int windowWHeight=windowSize.y;
        float windowScaleX = windowWidth/kWinWidth;
        float windowScaleY = windowWHeight/kWinHeight;
        
        sf::Vector2u imageSize = faceImage.getSize();
        double spriteScaleX = static_cast<double>(windowWidth)/imageSize.x;
        double spriteScaleY = static_cast<double>(windowWHeight)/imageSize.y;
        double spriteScale = min(spriteScaleX, spriteScaleY);
        
        faceTexture.loadFromImage(faceImage);

        faceSprite.setTexture(faceTexture);
       
        sf::Vector2u faceImageSize = faceImage.getSize();
        sf::IntRect subRect(0,0,faceImageSize.x,faceImageSize.y);
        faceSprite.setTextureRect(subRect);

        faceSprite.setScale(spriteScale/windowScaleX, spriteScale/windowScaleY);
        //faceSprite.Resize(windowWidth,windowWHeight);
    }
}


static void ImageResized(sf::RenderWindow &window, sf::Image &faceImage, sf::Sprite &faceSprite, int newWidth, int newHeight)
{
#if 0
    unsigned int width=window.GetWidth();
    unsigned int height=window.GetHeight();
    double scaleX = static_cast<double>(window.GetWidth())/faceImage.GetWidth();
    double scaleY = static_cast<double>(window.GetHeight())/faceImage.GetHeight();
    double scale = min(scaleX, scaleY);
    
    
    // The resize of the window already applied some scaling that applies to the window's content
    // so, we need to add a scaling to the sprite that cancels that in the direction where it is too large
    // The scaling applied is:
    // newSize/original size
    
    //scale = kWinSize/faceImage.GetHeight();
    //faceSprite.SetScale(scale, scale);
    faceSprite.SetImage(faceImage);
    
    sf::Vector2f spriteSize = faceSprite.GetSize();
#else
    sf::Vector2u windowSize = window.getSize();
    unsigned int windowWidth=windowSize.x;
    unsigned int windowWHeight=windowSize.y;
    float windowScaleX = windowWidth/kWinWidth;
    float windowScaleY = windowWHeight/kWinHeight;
    
    sf::Vector2u imageSize = faceImage.getSize();
    double spriteScaleX = static_cast<double>(windowWidth)/imageSize.x;
    double spriteScaleY = static_cast<double>(windowWHeight)/imageSize.y;
    double spriteScale = min(spriteScaleX, spriteScaleY);
    
    faceSprite.setScale(spriteScale/windowScaleX, spriteScale/windowScaleY);
#endif
}


////////////////////////////////////////////////////////////
/// Entry point of application
///
/// \return Application exit code
///
////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    // argv[0] is /Users/brice/MacDev/p4ws/Faces/bin/Faces.app/Contents/MacOS/Faces
    //filesystem::path appContents = filesystem::path(argv[0]).parent_path().parent_path();
    // current directory is Contents/Resources
    //filesystem::path crt=filesystem::current_path();
    fs::path mypath = fs::system_complete( fs::path( argv[0] ) ).branch_path();
    chdir((mypath/"../Resources").c_str());
    string exeName = strrchr(argv[0], '/')+1;
    
    // Get name and image paths
    Strings paths;
    string name;
    GetThumbPathsFast(name, paths);
    int nbImages = paths.size();

    // Create the window of the application
    string windowTitle(name + " (" + boost::lexical_cast<string>(nbImages) + ")");
    sf::RenderWindow mainWindow(sf::VideoMode(kWinWidth, kWinHeight, 32), windowTitle, sf::Style::Resize | sf::Style::Close);

    // Create a timer for pacing the image loop
    sf::Clock AITimer;

    // create the image loader and sprite
    sf::Image faceImage;
    sf::Texture faceTexture;
    sf::Sprite faceSprite;

    // Show the first image
    int imageIndex=0;
    ShowImage(mainWindow, faceImage, faceTexture, faceSprite, paths, imageIndex);

    typedef enum {Nop, Exit, ResizeImage, NextImage, PrevImage, ToggleDiaporama, SpeedUp, SlowDown} Action;
    
    // main loop
    bool diaporama=false;
    int diaporamaSpeedMs = 300;
    while (mainWindow.isOpen())
    {
        sf::Event::SizeEvent sizeEvent;
        
        Action action=Nop;
        
        // Control:
        // space: toggles diaporama
        // up/down arrows: change diaporama speed
        // left/right: turn off diaporama and change current image
        
        
        
        
        if (diaporama && AITimer.getElapsedTime().asMilliseconds() > diaporamaSpeedMs)
        {
            AITimer.restart();
            action = NextImage;
        }
        
        // Handle events
        sf::Event event;
        if (mainWindow.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                    // Window closed -> exit
                    action = Exit;
                    break;
                case sf::Event::KeyPressed:
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Key::Escape:
                            // escape key pressed -> exit
                            action=Exit;
                            break;
                        case sf::Keyboard::Key::Left:
                            action = diaporama? ToggleDiaporama: PrevImage;
                            break;
                        case sf::Keyboard::Key::Right:
                            action = diaporama? ToggleDiaporama: NextImage;
                            break;
                        case sf::Keyboard::Key::Up:
                            action = diaporama? SpeedUp: Nop;
                            break;
                        case sf::Keyboard::Key::Down:
                            action = diaporama? SlowDown: Nop;
                            break;
                        case sf::Keyboard::Key::Space:
                            action = ToggleDiaporama;
                            break;
                        default:
                            break;
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    action = ToggleDiaporama;
                    break;
                case sf::Event::Resized:
                {
                    sizeEvent = event.size;
                    action = ResizeImage;
                }
                    break;
                    
                default:
                    break;
            }
            
//            if (mainWindow.GetInput().IsMouseButtonDown(sf::Mouse::Left)
//                || mainWindow.GetInput().IsKeyDown(sf::Keyboard::Key::Right)
//                || mainWindow.GetInput().IsKeyDown(sf::Keyboard::Key::Space))
//            {
//                imageIndex = (imageIndex+1)%nbImages;
//                ShowImage(faceImage, faceSprite, paths, imageIndex);
//            }
//            if (mainWindow.GetInput().IsKeyDown(sf::Keyboard::Key::Left))
//            {
//                imageIndex = (imageIndex-1+nbImages)%nbImages;
//                ShowImage(faceImage, faceSprite, paths, imageIndex);
//            }

        }
        
        // Handle application events
        switch (action)
        {
            case Exit:
                mainWindow.close();
                break;
            case ResizeImage:
                ImageResized(mainWindow, faceImage, faceSprite, sizeEvent.width, sizeEvent.height);
                break;
            case NextImage:
                imageIndex = (imageIndex+1)%nbImages;
                ShowImage(mainWindow, faceImage, faceTexture, faceSprite, paths, imageIndex);
                break;
            case PrevImage:
                imageIndex = (imageIndex-1+nbImages)%nbImages;
                ShowImage(mainWindow, faceImage, faceTexture, faceSprite, paths, imageIndex);
                break;
            case ToggleDiaporama:
                diaporama=!diaporama;
                break;
            case SpeedUp:
                diaporamaSpeedMs = max<float>(100, diaporamaSpeedMs-100);
                break;
            case SlowDown:
                diaporamaSpeedMs = min<float>(2000, diaporamaSpeedMs+100);
                break;
                
            default:
                break;
        }

        // Refresh
        mainWindow.clear();
        mainWindow.draw(faceSprite);
        mainWindow.display();
    }

    return EXIT_SUCCESS;
}
