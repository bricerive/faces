----------------------------------------------------------------
Faces
----------------------------------------------------------------

################################################################
ToDo:
################################################################
Show count of images in selection lists
Show image index as overlay
Get static version of SFML
The CMake build works but it copies the frameworks too literally (does not keep soft links)
Rid SFML from X11 dependency? Use SDL?
Add morphing option
################################################################
Done:
################################################################

Resize window stay square
Add icon to generated app
start with first image
bigger window

installed SFML

installed the frameworks into /Library/Frameworks

installed the project templates but they don't work with XCode 4.0.1

installed V2.0 from source snapshot

Removed the frameworks from /Library/Frameworks and move them into Faces/Contents/Frameworks so that it stands alone

The display of varying thumb sizes is not working (try Papa ou Maman)

CMake build
----------------------------------------------------------------
----------------------------------------------------------------
to build under Yosemite:
use CMkae 3.0
rebuild boost
get rid of seticon target and use the one in MovieIndexer
----------------------------------------------------------------
SFML requires X11 - Yosemite does not have be by default -> requires an install
----------------------------------------------------------------
Does not work with Yosemite's Photo :-(
Need to reverse engineer the new format (and keep old code for backward compatibility?)
